//
//  Team.h
//  FIFA16
//
//  Created by user on 7/23/15.
//  Copyright (c) 2015 GECT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Team : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * attacking;
@property (nonatomic, retain) NSNumber * mid;
@property (nonatomic, retain) NSNumber * defence;

@end
