//
//  Team.m
//  FIFA16
//
//  Created by user on 7/23/15.
//  Copyright (c) 2015 GECT. All rights reserved.
//

#import "Team.h"


@implementation Team

@dynamic name;
@dynamic attacking;
@dynamic mid;
@dynamic defence;

@end
