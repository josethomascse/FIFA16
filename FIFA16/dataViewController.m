//
//  dataViewController.m
//  FIFA16
//
//  Created by user on 7/23/15.
//  Copyright (c) 2015 GECT. All rights reserved.
//

#import "dataViewController.h"
#import <CoreData/CoreData.h>


@interface dataViewController ()

@end

@implementation dataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (IBAction)save:(id)sender {
    NSString *text = _textfield1.text;
    NSLog(@"%@",text);
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSManagedObject *newTeam = [NSEntityDescription insertNewObjectForEntityForName:@"Player" inManagedObjectContext:context];
    [newTeam setValue:text forKey:@"name"];
    
//    int attval = [self.textfield2.text intValue];
//    [newTeam setValue:[NSNumber numberWithFloat:attval] forKey:@"attacking"];
//    
//    
//    int midval = [self.textfield3.text intValue];
//    [newTeam setValue:[NSNumber numberWithFloat:midval] forKey:@"mid"];
//    
//    int defval = [self.textfield4.text intValue];
//    [newTeam setValue:[NSNumber numberWithFloat:defval] forKey:@"defence"];
    
    NSString *text1 = _textfield5.text;
    NSLog(@"%@",text1);
    [newTeam setValue:text1 forKey:@"teamname"];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
    
    
    
    
