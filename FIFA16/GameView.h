//
//  GameView.h
//  FIFA16
//
//  Created by user on 7/26/15.
//  Copyright (c) 2015 GECT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GamePlayer.h"

enum state{
    position,
    start,
    play,
    end
};

enum ballStatus{
    up,
    down,
    left,
    right,
    idle,
    goal
};

@interface GameView : UIView

@property (retain,nonatomic) UIView *circleView;
@property (retain,nonatomic) UIView *playGround;
@property (retain,nonatomic) UIView *goalPost1;
@property (retain,nonatomic) UIView *goalPost2;
@property (retain,nonatomic) UIView *midLine;
@property (retain,nonatomic) UIView *goalBox1;
@property (retain,nonatomic) UIView *goalBox2;
@property (retain,nonatomic) UIImageView *footBall;
@property (retain,nonatomic) NSMutableArray *players;
@property int goal1;
@property int goal2;

-(void)animateBall;
@end
