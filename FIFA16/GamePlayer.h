//
//  Player.h
//  FIFA16
//
//  Created by user on 7/26/15.
//  Copyright (c) 2015 GECT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GamePlayer : NSObject

@property (retain,nonatomic) UIView *playerView;
@property (retain,nonatomic) UIView *borderView;
@property (retain,nonatomic) UIView *panView;
@property (retain,nonatomic) IBOutlet UIPanGestureRecognizer *playerGesture;
@property float x;
@property float y;
@property float currentX;
@property float currentY;
@property int team;
@property int pid;

@end
