//
//  ViewController.m
//  FIFA16
//
//  Created by user on 7/25/15.
//  Copyright (c) 2015 GECT. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"StartScreen" owner:self options:nil];
     UIView *mainView = [subviewArray objectAtIndex:0];
    [self.view addSubview:mainView];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
