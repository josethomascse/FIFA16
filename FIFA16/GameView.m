//
//  GameView.m
//  FIFA16
//
//  Created by user on 7/26/15.
//  Copyright (c) 2015 GECT. All rights reserved.
//

#import "GameView.h"

@implementation GameView

int i=0;
float x,y;
bool playerAdd=true;
enum state currentState;
enum ballStatus ballState=idle;
float m=0;
float refX=0,refY=0;
NSTimer *timer;
GamePlayer *player;
int change=0,direction=0;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.*/
- (void)drawRect:(CGRect)rect {
   
    self.playGround = [[UIView alloc] initWithFrame:CGRectMake(self.center.x-180,self.center.y-310,360,620)];
    self.playGround.alpha = 1;
    self.playGround.layer.borderWidth=2;
    self.playGround.layer.borderColor=[[UIColor whiteColor]CGColor];
    self.playGround.backgroundColor=[UIColor colorWithRed:53.0/255.0 green:152.0/255.0 blue:31.0/255.0 alpha:1.0];
    self.playGround.clipsToBounds=YES;
    [self addSubview:self.playGround];
    
    self.circleView = [[UIView alloc] initWithFrame:CGRectMake(self.center.x-55,self.center.y-55,110,110)];
    self.circleView.alpha = 1;
    self.circleView.layer.cornerRadius = 55;
    self.circleView.layer.borderWidth=2;
    self.circleView.layer.borderColor=[[UIColor whiteColor]CGColor];
    [self addSubview:self.circleView];
    
    self.goalPost1 = [[UIView alloc] initWithFrame:CGRectMake(self.center.x-60,self.center.y-328,120,20)];
    self.goalPost1.alpha = 1;
    self.goalPost1.layer.borderWidth=2;
    self.goalPost1.layer.borderColor=[[UIColor whiteColor]CGColor];
    [self addSubview:self.goalPost1];
    
    self.goalPost2 = [[UIView alloc] initWithFrame:CGRectMake(self.center.x-60,self.center.y+308,120,20)];
    self.goalPost2.alpha = 1;
    self.goalPost2.layer.borderWidth=2;
    self.goalPost2.layer.borderColor=[[UIColor whiteColor]CGColor];
    [self addSubview:self.goalPost2];
    

    self.goalBox2 = [[UIView alloc] initWithFrame:CGRectMake(self.center.x-120,self.center.y+210,240,100)];
    self.goalBox2.alpha = 1;
    self.goalBox2.layer.borderWidth=2;
    self.goalBox2.layer.borderColor=[[UIColor whiteColor]CGColor];
    [self addSubview:self.goalBox2];
    
    self.goalBox1 = [[UIView alloc] initWithFrame:CGRectMake(self.center.x-120,self.center.y-310,240,100)];
    self.goalBox1.alpha = 1;
    self.goalBox1.layer.borderWidth=2;
    self.goalBox1.layer.borderColor=[[UIColor whiteColor]CGColor];
    [self addSubview:self.goalBox1];

    self.midLine = [[UIView alloc] initWithFrame:CGRectMake(self.center.x-180,self.center.y-1,360,2)];
    self.midLine.alpha = 1;
    self.midLine.backgroundColor=[UIColor whiteColor];
    [self addSubview:self.midLine];

    self.footBall= [[UIImageView alloc] initWithFrame:CGRectMake(self.center.x-8,self.center.y-8,16,16)];
    self.footBall.alpha = 1;
    self.footBall.layer.cornerRadius=8;
    [self.footBall setContentMode:UIViewContentModeScaleAspectFit];
    self.footBall.image=[UIImage imageNamed:@"football.png"];
    [self addSubview:self.footBall];

    self.players=[[NSMutableArray alloc]init];
    
    [self createPlayer:188.0 second:42.5];
    [self createPlayer:188.0 second:624.5];
    [self createPlayer:188.0 second:296];
    [self createPlayer:188.0 second:372];

    currentState=position;
    refX=self.footBall.center.x;
    refY=self.footBall.center.y;
    self.goal1=0;
    self.goal2=0;
    self.playGround.clipsToBounds=YES;
}

- (IBAction)movePlayer:(id)sender {
    if(currentState==start){
        currentState=play;
        [self performSelector:@selector(gameEnd) withObject:self afterDelay:180];
    }
    if(currentState == play){
      
        UIPanGestureRecognizer *panGesture=sender;
    
        GamePlayer *player=[self.players objectAtIndex:panGesture.view.tag];
//        NSLog(@"%d %d",player.x,player.y);
        if(panGesture.state == UIGestureRecognizerStateBegan){
            [UIView animateWithDuration:0.5 animations:^{
                player.panView.backgroundColor=[UIColor colorWithRed:0.0 green:0.8 blue:0.0 alpha:0.5];
            }];
        }
        CGPoint translation=[panGesture translationInView:self];
        int x,y,distance;
        x=panGesture.view.center.x+translation.x;
        y=panGesture.view.center.y+translation.y;
        distance=((player.x-x)*(player.x-x) + (player.y-y)*(player.y-y));
        if(distance<2500){
            GamePlayer *p =[[GamePlayer alloc]init];
            int can=1;
            for(int j=2;j<14;j++){
                p=[self.players objectAtIndex:j];
                if(player.pid == p.pid)
                    continue;
                distance=((p.currentX-x)*(p.currentX-x) + (p.currentY-y)*(p.currentY-y));
                if(distance<900){
                    can=0;
                    break;
                }
            }
            if(can){
                panGesture.view.center=CGPointMake(x,y);
                [panGesture setTranslation:CGPointMake(0,0) inView:self];
                player.currentX=x;
                player.currentY=y;
            }
        }
        distance=((self.footBall.center.x-x)*(self.footBall.center.x-x) + (self.footBall.center.y-y)*(self.footBall.center.y-y));
       if(ballState == idle){
           if(distance<529){
               x=self.footBall.center.x;
               y=self.footBall.center.y;
               if((player.currentX-x)!=0)
                   m=((1000-player.currentY)-(1000-y))/(player.currentX-x);
               else
                   m=99999;
              // NSLog(@"%f %f %f",m,player.currentX,player.borderView.center.x);
               if(m>-1 && m<1){
                   if(player.currentX>x)
                       ballState=left;
                   else
                       ballState=right;
               }else{
                   if(player.currentY>y)
                       ballState=up;
                   else
                       ballState=down;
               }
               refX=self.footBall.center.x;
               refY=self.footBall.center.y;
           }

        }
        if(panGesture.state == UIGestureRecognizerStateEnded){
            [UIView animateWithDuration:0.5 animations:^{
            player.panView.backgroundColor=[UIColor clearColor];
            }];
        }
    }
}

- (IBAction)tapfield:(id)sender {
    CGPoint tapPoint = [sender locationInView:self];
    NSLog(@"%f %f",tapPoint.x,tapPoint.y);
    if(currentState == position){
        if(tapPoint.x<21 || tapPoint.x>354 || tapPoint.y<41 || tapPoint.y>626)
            return;
        if(tapPoint.x>51.5 && tapPoint.x<324 && tapPoint.y<138)
            return;
        if(tapPoint.x>51.5 && tapPoint.x<324 && tapPoint.y>528)
            return;
        int distance;
        x=self.center.x;
        y=self.center.y;
        distance=((tapPoint.x-x)*(tapPoint.x-x) + (tapPoint.y-y)*(tapPoint.y-y));
        if(distance<4500)
            return;
        GamePlayer *p =[[GamePlayer alloc]init];
        for(int j=4;j<i;j++){
            p=[self.players objectAtIndex:j];
            x=p.x;
            y=p.y;
            distance=((tapPoint.x-x)*(tapPoint.x-x) + (tapPoint.y-y)*(tapPoint.y-y));
            if(distance<900)
                return;
        }
        if(i<14){
            [self createPlayer:tapPoint.x second:tapPoint.y];
        }
        if(i==14){
            currentState=start;
            timer=[NSTimer timerWithTimeInterval:0.007 target:self selector:@selector(animateBall) userInfo:nil repeats:YES];
            [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
//        [UIView animateWithDuration:1 animations:^{
//            self.footBall.center = CGPointMake(tapPoint.x, tapPoint.y);
//            self.footBall.transform =CGAffineTransformRotate(_footBall.transform, 3.5);
//        }completion:^(BOOL finished){}];
//      [UIView animateWithDuration:.5
//                          delay:1.1
//                        options:nil
//                     animations:^{
//                     }
//                     completion:^(BOOL finished){}];
    
        }
    }
}

-(void)animateBall{
    if(ballState !=idle){
        int count=0;
    do
    {
    change=0;
    direction=0;
    x=self.footBall.center.x;
    y=self.footBall.center.y;
    if(ballState == up){
        y--;
        x=(((1000-y)-(1000-refY))/m)+refX;
        if(y>34){
            if(x<358){
                if(x>16){
                    if(![self isCollisionDetected])
                        change=1;
                }
                else{
                    direction=1;
                }
            }
            else{
                direction=1;
            }
        }
        else{
            if(!(x<238 && x>138)){
                ballState = down;
                direction=1;
            }
            else{
                change=1;
            }
            if(y<16){
                [self goalOccured];
                self.goal1++;
                [timer invalidate];
                
            }
        }
    }
    else if(ballState == down){
        y++;
        x=(((1000-y)-(1000-refY))/m)+refX;
        if(y<634){
            if(x>16){
                if(x<358){
                    if(![self isCollisionDetected])
                        change=1;
                }
                else{
                    direction=1;
                }
            }
            else{
                direction=1;
            }
        }
        else{
            if(!(x<238 && x>138)){
                ballState = up;
                direction=1;
            }
            else{
                change=1;
            }
            if(y>652){
                [self goalOccured];
                self.goal2++;
                [timer invalidate];
                
            }
        }
    }
    else if(ballState == left){
        x--;
        y=1000-(m*(x-refX))-(1000-refY);
        if(x>16){
            if(y<634){
                if(y>34){
                    if(![self isCollisionDetected])
                        change=1;
                }
                else{
                    if(!(x<238 && x>138)){
                        direction=1;
                    }
                    else{
                        change=1;
                    }
                    if(y<16){
                        [self goalOccured];
                        self.goal1++;
                        [timer invalidate];
                        
                    }

                }
            }
            else{
                if(!(x<238 && x>138)){
                    direction=1;
                }
                else{
                    change=1;
                }
                if(y>652){
                    [self goalOccured];
                    self.goal2++;
                    [timer invalidate];
                    
                }
            }
        }
        else{
            ballState = right;
            direction=1;
        }
    }
    else if(ballState == right){
        x++;
        y=1000-(m*(x-refX))-(1000-refY);
        if(x<358){
            if(y<634){
                if(y>34){
                    if(![self isCollisionDetected])
                        change=1;
                }
                else{
                    if(!(x<238 && x>138)){
                        direction=1;
                    }
                    else{
                        change=1;
                    }
                    if(y<16){
                        [self goalOccured];
                        self.goal1++;
                        [timer invalidate];
                        
                    }

                }
            }
            else{
                if(!(x<238 && x>138)){
                    direction=1;
                }
                else{
                    change=1;
                }
                if(y>652){
                    [self goalOccured];
                    self.goal2++;
                    [timer invalidate];
                    
                }

            }
        }
        else{
            ballState = left;
            direction=1;
        }
    }
        
        if(direction){
            m=-1*m;
            refX=self.footBall.center.x;
            refY=self.footBall.center.y;
        }
        count++;
    }while((!change) && (count<3));
    self.footBall.center=CGPointMake(x,y);
}
}

-(void)createPlayer:(float)posx second:(float)posy{
    GamePlayer *player=[[GamePlayer alloc]init];
    player.panView = [[UIView alloc] initWithFrame:CGRectMake(posx-58,posy-73,100,100)];
    player.panView.layer.cornerRadius=50;
    player.panView.backgroundColor=[UIColor clearColor];
    [self.playGround addSubview:player.panView];
    if(i<2){
        player.borderView = [[UIView alloc] initWithFrame:CGRectMake(posx-40,posy-40,80,80)];
        player.borderView.layer.cornerRadius=40;
    }
    else{
        player.borderView = [[UIView alloc] initWithFrame:CGRectMake(posx-30,posy-30,60,60)];
        player.borderView.layer.cornerRadius=30;
    }
    player.borderView.backgroundColor=[UIColor clearColor];
    player.playerGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(movePlayer:)];
    [player.borderView addGestureRecognizer:player.playerGesture];
    if(i<2){
        player.playerView = [[UIView alloc] initWithFrame:CGRectMake(25,25,30,30)];
    }else{
        player.playerView = [[UIView alloc] initWithFrame:CGRectMake(15,15,30,30)];
    }
    player.playerView.layer.cornerRadius=15;
    [player.borderView addSubview:player.playerView];
    player.x=player.borderView.center.x;
    player.y=player.borderView.center.y;
    player.currentX=player.x;
    player.currentY=player.y;
    player.borderView.tag=i;
    player.pid=i;

    if(playerAdd){
        player.playerView.backgroundColor=[UIColor blueColor];
        player.team=0;
        playerAdd=false;
    }
    else{
        player.playerView.backgroundColor=[UIColor redColor];
        player.team=1;
        playerAdd=true;
    }
    
    [self.players addObject:player];
    [self addSubview:player.borderView];
    i++;
}

-(BOOL)isCollisionDetected{
    player =[[GamePlayer alloc]init];
    int can=0,distance;
    for(int j=0;j<14;j++){
        player=[self.players objectAtIndex:j];
        distance=((player.currentX-x)*(player.currentX-x) + (player.currentY-y)*(player.currentY-y));
        if(distance<529){
            can=1;
            float m2;
            if(player.currentY!=y)
                m2=(-1*(player.currentX-x))/((1000-player.currentY)-(1000-y));
            else
                m2=99999;
            m=((2*m2)-((1-m2)*(1-m2)*m))/(1-(m2*m2)+(2*m*m2));
            if(m>-1 && m<1){
                if(player.currentX>x)
                    ballState=left;
                else
                    ballState=right;
            }else{
                if(player.currentY>y)
                    ballState=up;
                else
                    ballState=down;
            }
            refX=self.footBall.center.x;
            refY=self.footBall.center.y;
            direction=0;
            break;
        }
    }
    if(can){
        [UIView animateWithDuration:0.5 animations:^{
            player.borderView.backgroundColor=[UIColor colorWithRed:0.0 green:0.8 blue:0.0 alpha:0.5];
        }];
        [UIView animateWithDuration:0.5
                              animations:^{
                                  player.borderView.backgroundColor=[UIColor clearColor];
                              }
                              completion:^(BOOL finished){}];
    }
    return can;
}
UILabel *goalLabel;
-(void)goalOccured{
    goalLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 250,300, 100)];
    goalLabel.center=CGPointMake(self.center.x, self.center.y);
    goalLabel.text=@"G O A L ! ";
    goalLabel.textColor=[UIColor whiteColor];
    [goalLabel setFont:[UIFont fontWithName:@"TrebuchetMS" size:60]];
    [goalLabel setTextAlignment:NSTextAlignmentCenter];
    [UIView animateWithDuration:2 animations:^{
        [self addSubview:goalLabel];
    }];

    [self performSelector:@selector(setStart) withObject:self afterDelay:2.0];
}

-(void)setStart{
    [goalLabel removeFromSuperview];
    for(int j=0;j<14;j++){
        player=[self.players objectAtIndex:j];
        player.currentX=player.x;
        player.currentY=player.y;
        player.borderView.center=CGPointMake(player.currentX,player.currentY);
    }
    self.footBall.center=CGPointMake(self.center.x,self.center.y);
    ballState=idle;
    timer=[NSTimer timerWithTimeInterval:0.005 target:self selector:@selector(animateBall) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];    
}

-(void)gameEnd{
    [timer invalidate];
    goalLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 220,300, 70)];
    goalLabel.center=CGPointMake(self.center.x, self.center.y-40);
    goalLabel.text=[NSString stringWithFormat:@"RED BLUE"];
    goalLabel.textColor=[UIColor whiteColor];
    [goalLabel setFont:[UIFont fontWithName:@"TrebuchetMS" size:60]];
    [goalLabel setTextAlignment:NSTextAlignmentCenter];
    [UIView animateWithDuration:2 animations:^{
        [self addSubview:goalLabel];
    }];
    UILabel *ggoalLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 280,300, 70)];
    ggoalLabel.center=CGPointMake(self.center.x, self.center.y+40);
    ggoalLabel.text=[NSString stringWithFormat:@"%d     %d",self.goal1,self.goal2];
    ggoalLabel.textColor=[UIColor whiteColor];
    [ggoalLabel setFont:[UIFont fontWithName:@"TrebuchetMS" size:60]];
    [ggoalLabel setTextAlignment:NSTextAlignmentCenter];
    [UIView animateWithDuration:2 animations:^{
        [self addSubview:ggoalLabel];
    }];

    

}

@end

