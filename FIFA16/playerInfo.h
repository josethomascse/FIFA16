//
//  playerInfo.h
//  FIFA16
//
//  Created by user on 7/23/15.
//  Copyright (c) 2015 GECT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface playerInfo : UIView
@property (strong, nonatomic) IBOutlet UILabel *team1;

@property (strong, nonatomic) IBOutlet UILabel *coach1;
@property int team;
@property int teamTwo;
@property (strong, nonatomic) IBOutlet UILabel *team2;
@property (strong, nonatomic) IBOutlet UILabel *coach2;
@property (strong, nonatomic) IBOutlet UIProgressView *progress;

-(void)c;
@end
