//
//  StartScreenView.m
//  FIFA16
//
//  Created by user on 7/31/15.
//  Copyright (c) 2015 GECT. All rights reserved.
//

#import "StartScreenView.h"
#import "teamselect.h"

@implementation StartScreenView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (IBAction)kickoff:(id)sender {
    
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"TeamSelect" owner:self options:nil];
    teamselect *playerView=[subviewArray objectAtIndex:0];
    playerView.num=-1;
    playerView.num2=-1;
    
    [self.superview addSubview:playerView];
    [self removeFromSuperview];

}
- (IBAction)friendly:(id)sender {
}
- (IBAction)tournament:(id)sender {
}
- (IBAction)penalty:(id)sender {
}
- (IBAction)exit:(id)sender {
}

@end
