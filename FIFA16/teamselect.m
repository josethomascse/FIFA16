//
//  teamselect.m
//  FIFA16
//
//  Created by user on 7/23/15.
//  Copyright (c) 2015 GECT. All rights reserved.
//

#import "teamselect.h"
#import <CoreData/CoreData.h>
#import "Team.h"
#import "playerInfo.h"


@interface teamselect()

-(void)datafunc;

@property (strong) NSMutableArray *teamArray;

@end

@implementation teamselect

Team *t;
NSString *temp;

-(void)datafunc{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Team"];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                                   ascending:YES];
    NSMutableArray *sortDescriptors = [[NSMutableArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSError *error = nil;
    
    self.teamArray = [[managedObjectContext executeFetchRequest:fetchRequest error:&error]mutableCopy];
    NSLog(@"%lu", (unsigned long)self.teamArray.count);
}

// Database fetch
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    
    //database fetching code
    [self datafunc];
    
    t= [self.teamArray objectAtIndex:0];
    NSLog(@"%@",t.name);
    self.teamnamelabel.text = t.name;
    
    temp = [NSString stringWithFormat:@"%d",[t.attacking intValue]];
    self.attlabel.text = temp;
    temp = [NSString stringWithFormat:@"%d",[t.mid intValue]];
    self.midlabel.text = temp;
    temp = [NSString stringWithFormat:@"%d",[t.defence intValue]];
    self.deflabel.text = temp;
    
    self.jerView.image = [UIImage imageNamed:@"argentinajer"];
    //self.teamnamelabel.text =
    
   

}






int a = 0;


- (IBAction)rightClicked:(id)sender {
    a++;
    //a = a % 8;
    if (a==8) {
        a = 0;
    }
    printf("%d",a);
    t= [self.teamArray objectAtIndex:a];
    NSLog(@"%@",t.name);
    self.teamnamelabel.text = t.name;
    
    temp = [NSString stringWithFormat:@"%d",[t.attacking intValue]];
    self.attlabel.text = temp;
    temp = [NSString stringWithFormat:@"%d",[t.mid intValue]];
    self.midlabel.text = temp;
    temp = [NSString stringWithFormat:@"%d",[t.defence intValue]];
    self.deflabel.text = temp;

    
    switch (a) {
        case 0 :    self.jerView.image = [UIImage imageNamed:@"argentinajer"];
            break;
            
        case 1 :    self.jerView.image = [UIImage imageNamed:@"brasiljer"];
            break;
            
        case 2 :    self.jerView.image = [UIImage imageNamed:@"englandjer"];
            break;
            
        case 3 :    self.jerView.image = [UIImage imageNamed:@"francejer"];
            break;
            
        case 4 :    self.jerView.image = [UIImage imageNamed:@"germanyjer"];
            break;
            
        case 5 :    self.jerView.image = [UIImage imageNamed:@"italyjer"];
            break;
            
        case 6 :    self.jerView.image = [UIImage imageNamed:@"netherlandsjer"];
            break;
            
        case 7 :    self.jerView.image = [UIImage imageNamed:@"qburstjer"];
            break;
            
            
        default:
            break;
    }
    
}
- (IBAction)leftClicked:(id)sender {
    a--;
    if (a==-1) {
        a = 7;
    }
    printf("%d",a);
    t= [self.teamArray objectAtIndex:a];
    NSLog(@"%@",t.name);
    self.teamnamelabel.text = t.name;
    temp = [NSString stringWithFormat:@"%d",[t.attacking intValue]];
    self.attlabel.text = temp;
    temp = [NSString stringWithFormat:@"%d",[t.mid intValue]];
    self.midlabel.text = temp;
    temp = [NSString stringWithFormat:@"%d",[t.defence intValue]];
    self.deflabel.text = temp;
    
    switch (a) {
        case 0 :    self.jerView.image = [UIImage imageNamed:@"argentinajer"];
                    break;
            
        case 1 :    self.jerView.image = [UIImage imageNamed:@"brasiljer"];
                    break;
            
        case 2 :    self.jerView.image = [UIImage imageNamed:@"englandjer"];
                    break;
            
        case 3 :    self.jerView.image = [UIImage imageNamed:@"francejer"];
                    break;
            
        case 4 :    self.jerView.image = [UIImage imageNamed:@"germanyjer"];
                    break;
            
        case 5 :    self.jerView.image = [UIImage imageNamed:@"italyjer"];
                    break;
         
        case 6 :    self.jerView.image = [UIImage imageNamed:@"netherlandsjer"];
                    break;
            
        case 7 :    self.jerView.image = [UIImage imageNamed:@"qburstjer"];
                    break;
            
            
        default:
            break;
    }

}

- (IBAction)next:(id)sender {
//    playerInfo *playerView=[[playerInfo alloc]init];
//    playerView.team=a;
//    [self.superview addSubview:playerView];
//    [self removeFromSuperview];
    NSLog(@"%d %d",self.num,self.num2);
    if(self.num==-1){
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"TeamSelect" owner:self options:nil];
        teamselect *playerView=[subviewArray objectAtIndex:0];
        playerView.num=a;
        
        [self.superview addSubview:playerView];
        [self removeFromSuperview];

    }
    else{
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"PlaySettings" owner:self options:nil];
        playerInfo *playerView=[subviewArray objectAtIndex:0];
        playerView.team=self.num;
        playerView.teamTwo=a;
        [self.superview addSubview:playerView];
        [self removeFromSuperview];
    }
    
}


@end
