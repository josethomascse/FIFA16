//
//  Player.h
//  FIFA16
//
//  Created by user on 7/28/15.
//  Copyright (c) 2015 GECT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Player : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * teamname;

@end
